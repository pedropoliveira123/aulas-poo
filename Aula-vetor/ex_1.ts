//Crie um array com 5 números. Em seguida, use um loop for para iterar sobre o array e exibir a soma de todos os números.
namespace ex_1
{
    let numeros: number[] = [8, 10, 11, 21, 77];
    let desc: string[] = ["Numero1", "Numero2", "Numero3", "Numero4", "Numero5"];

    let qtdNumeros: number = numeros.length;
    let somaNumeros: number = 0;
    for (let i = 0; i < numeros.length; i++) {
        somaNumeros += numeros[i]
        //somaNumeros = somaNumeros + numeros[i];
    }
    let result: number;
    result = somaNumeros;
    for (let i = 0; i < numeros.length; i++) {
        console.log(`${desc[i]} = ${numeros[i]} `);
    }
    console.log(`A soma final é: ${result}`);
    
}
//Crie um array com 6 números. Em seguida, use o método filter() para criar um novo array contendo apenas os números ímpares.

namespace numeros_Impares
{
    let numeros: number[] = [7,10,14,20,21,77];
    let impares = numeros.filter(function(num) {
        return num % 2 != 0; });
    console.log(impares); 
}
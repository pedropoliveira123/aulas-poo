//Crie um array com 4 objetos, cada um representando um livro com as propriedades titulo e autor. Em seguida, use o método map() para criar um novo array contendo apenas os títulos dos livros.
namespace ex_3
{
    let book: any[] = [ {titulo: "Turma da Mônica", autor: "Mauricio de Souza"}, {titulo: "Senhor dos Anéis", autor: "J. R. R. Tolkien"}, {titulo: "Harry Potter", autor: "J.K Rowling"}, {titulo: "Diario de um banana", autor: "Jeff Kinney"}, {titulo:"Sitio do Picapau Amarelo", autor:"Monteiro Lobato"}, {titulo: "O saci", autor:"Monteiro Lobato"} ];
    let titulos = book.map(function(titulo) {
        return titulo.titulo;
      });
      //let titulos = book.map((book) =>           {
      //  return book.titulo
      //});
      
      console.log(titulos);

      //Dado um array de objetos livros, contendo os campos titulo e autor, crie um programa em TypeScript que utilixe a função filter() para encontrar todos os livros do autor com valor "Autor 3". Em seguida, utilize a função map() para mostrar apenas os titulos dos livros encontrados. O resultado deve ser exibido no console.
      let monteiro = book.filter((book) =>
      {
        return book. autor == "Monteiro Lobato"})

      console.log(monteiro);
      
      let titulo = monteiro.map(function(monteiro) {
        return monteiro.titulo;
      });
      console.log(titulo);
      
    } 
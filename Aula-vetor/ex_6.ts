//Crie um vetor chamado "alunos" contendo três objetos, cada um representando um aluno com as seguintes propriedades: "nome" (string), "idade" (number) e "notas" (array de numeros).Preencha o vetor com informações ficticias.
//Em seguida percorra o vetor utilizando a função "forEach" e para cada aluno,calcule a media das notas e imprima  o resultado na tela,juntamente com o nome e a idade do aluno.
namespace ex_6{
    interface Alunos {
        nome: string;
        idade: number;
        notas: number[];
    }
    const alunos: Alunos[] = [
        {nome: "Pedro", idade: 16, notas:[8, 7, 10]}, 
        {nome: "André", idade: 13, notas:[8, 7, 8]},
        {nome: "Daniel", idade: 8, notas:[7, 5, 8]}
    ]
    alunos.forEach((aluno) => {
        let media = aluno.notas.reduce((total,nota) => {return total + nota})/aluno.notas.length
        if (media >= 7) {
            console.log(`A média do aluno ${aluno.nome} é igual ${media} está aprovado!`);
        }
        else {
            console.log(`A média do aluno ${aluno.nome} é igual ${media} está reprovado`);
        }
    })
}
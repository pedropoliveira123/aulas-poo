//Crie um array com 3 nomes de frutas. Em seguida, use um loop while para iterar sobre o array e exibir cada fruta em uma linha separada.
namespace ex_2
{
    let frutas: string[] = ["Kiwi", "Pitaia", "Limão"];
    let i: number = 0;

while (i < frutas.length) {
  console.log(frutas[i]);
  i++;
}
}
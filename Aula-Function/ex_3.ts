//Crie uma função que receba um array de números como parâmetro e retorne a soma desses números.
namespace ex_3 {
    function somaArray(numeros: number []) {
        let soma: number = 0;
        for(let i=0; i<numeros.length; i++){
            soma = soma + numeros[i]
        }
        return soma;
    }
    let resultado = somaArray([4, 8, 9, 55])
    console.log(resultado);
    
}